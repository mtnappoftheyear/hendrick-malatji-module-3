import 'package:flutter/material.dart';

void main() {
  runApp(const Editprofile());
}

class Editprofile extends StatelessWidget {
  const Editprofile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Edit Profile Page',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Edit Profile Page'),
        ),
        body: const Center(
          child: Text('Edit Profile Page'),
        ),
      ),
    );
  }
}