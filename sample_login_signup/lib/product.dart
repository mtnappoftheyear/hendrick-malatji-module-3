import 'package:flutter/material.dart';

void main() {
  runApp(const Product());
}

class Product extends StatelessWidget {
  const Product({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Product',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Product'),
        ),
        body: const Center(
          child: Text('Product'),
        ),
      ),
    );

    //   Container(
    //       padding: const EdgeInsets.only(top: 100),
    //       height: 200,
    //       decoration: BoxDecoration(
    //           image: DecorationImage(
    //               // ignore: prefer_const_constructors
    //               image: AssetImage("assets/fish1.jpg , assets/fish2.jpg,
    //                   fish3.jpg"),
    //               fit: BoxFit.fitHeight)));
    // }
  }
}
