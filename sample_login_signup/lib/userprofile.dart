import 'package:flutter/material.dart';

void main() {
  runApp(const Userprofile());
}

class Userprofile extends StatelessWidget {
  const Userprofile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Your Profile',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Your Profile'),
        ),
        body: const Center(
          child: Text('Your Profile'),
        ),
      ),
    );
  }
}
